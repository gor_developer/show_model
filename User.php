<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    const TYPE_EMPLOYEE = 'employee';
    const TYPE_EMPLOYER = 'employer';
    const TITLE_MR = 'mr';
    const TITLE_MRS = 'mrs';
    const FOLDER_PREFIX = 'user_';
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_DELETED = 3;
    const POSTS_LIMITED = 0;
    const POSTS_UNLIMITED = 1;
    const POSTS_TRIAL = 2;
    const INDEX_ON = 1;
    const INDEX_OFF = 0;

    protected $hidden = ['password', 'remember_token'];
    protected $fillable = ['firstname', 'lastname', 'email', 'user_number', 'title', 'birth_date',
        'zip_code', 'phone', 'country', 'company_name', 'company_name_original', 'subdomain', 'indexing', 'fb_id', 'confirm_token', 'education_id'];
    public $rules = [
        'firstname' => 'Required|Min:2|Max:32',
        'lastname' => 'Required|Min:2|Max:32',
        'email' => 'Required|Between:3,50|Email|Unique:users',
        'password' => 'Required|Between:6,32|Confirmed',
        'password_confirmation' => 'Required|Between:6,32',
        'title' => 'Required|Alpha|Min:2|Max:3',
        'position' => 'Min:2|Max:64',
        'photo' => 'Max:64',
        'birth_date' => 'Required|Date|date_format:"d.m.Y"|Before:2002-01-01',
        'zip_code' => 'Required|Numeric',
        'country' => 'Required|Alpha|Max:32',
        'phone' => 'Required|Max:32',
        'agree' => 'Required',
        'company_name' => 'Required:Max:205|Unique:users',
        'education_id' => 'Numeric|Exists:skill_type_values,id',
    ];
    public $messages = [
        'agree.required' => 'You need to accept Terms and Conditions to register',
        'true_password' => 'Please enter true password!',
        'company_name_exists_create' => 'The company name has already been taken.',
        'company_name_exists' => 'The company name has already been taken.'
    ];

    public static function boot() {
        parent::boot();
        static::saving(function($model) {
            $model->birth_date = (isset($model->birth_date)) ? date('Y-m-d', strtotime($model->birth_date)) : NULL;
        });
    }

    public function getBirthDateAttribute() {
        return (isset($this->attributes['birth_date'])) ? date('d.m.Y', strtotime($this->attributes['birth_date'])) : NULL;
    }

    public function validate($input, $case) {
        $rules = [];
        $dt = new Carbon\Carbon();
        switch ($case) {
            case 'create':
                $rules['email'] = $this->rules['email'];
                $rules['password'] = $this->rules['password'];
                $rules['password_confirmation'] = $this->rules['password_confirmation'];
                $rules['agree'] = $this->rules['agree'];
                break;
            case 'quick_create':
                $rules['email'] = $this->rules['email'];
                $rules['password'] = $this->rules['password'];
                $rules['password_confirmation'] = $this->rules['password_confirmation'];
                break;
            case 'update':
                $rules['title'] = $this->rules['title'];
                $rules['firstname'] = $this->rules['firstname'];
                $rules['lastname'] = $this->rules['lastname'];
                $rules['birth_date'] = 'Required|Date|date_format:"d.m.Y"|Before:' . $dt->subYears(13)->format('Y-m-d');
                $rules['email'] = 'Required|Between:3,50|Email';
                $rules['phone'] = $this->rules['phone'];
                $rules['zip_code'] = $this->rules['zip_code'];
                $rules['country'] = $this->rules['country'];
                break;
            case 'change_password':
                $rules['email'] = 'Required|Between:3,50|Email';
                $rules['password'] = $this->rules['password'];
                $rules['password_confirmation'] = $this->rules['password_confirmation'];
                break;
            case 'create_company':
                $rules['firstname'] = $this->rules['firstname'];
                $rules['lastname'] = $this->rules['lastname'];
                $rules['phone'] = $this->rules['phone'];
                $rules['password'] = $this->rules['password'];
                $rules['password_confirmation'] = $this->rules['password_confirmation'];
                $rules['email'] = $this->rules['email'];
                $rules['company_name'] = 'Required:Max:255|Unique:users|company_name_exists_create';
                $rules['company_name'] = 'Required:Max:255|company_name_exists_create';
                $rules['title'] = $this->rules['title'];
                $rules['position'] = $this->rules['position'];
                $rules['agree'] = $this->rules['agree'];
                break;
            case 'create_company_demo':
                $rules['firstname'] = $this->rules['firstname'];
                $rules['lastname'] = $this->rules['lastname'];
                $rules['phone'] = $this->rules['phone'];
                $rules['email'] = $this->rules['email'];
                $rules['company_name'] = $this->rules['company_name'];
                $rules['title'] = $this->rules['title'];
                $rules['position'] = $this->rules['position'];
                break;
            case 'change_password_company':
                $rules['email'] = 'Required|Between:3,50|Email';
                $rules['password'] = $this->rules['password'];
                $rules['password_confirmation'] = $this->rules['password_confirmation'];
                break;
            case 'change_password_hisself':
                $rules['old_password'] = 'Required|true_password';
                $rules['new_password'] = $this->rules['password'];
                $rules['new_password_confirmation'] = $this->rules['password_confirmation'];
                break;
            case 'update_company':
                $rules['firstname'] = $this->rules['firstname'];
                $rules['lastname'] = $this->rules['lastname'];
                $rules['company_name'] = 'Required:Max:255|Unique:users|company_name_exists';
                $rules['email'] = 'Required|Between:3,50|Email';
                $rules['phone'] = $this->rules['phone'];
                break;
        }
        
        Validator::extend('true_password', function($field, $value, $parameters) {
            if (Hash::check(Input::get('old_password'), Auth::user()->get()->password)) {
                return true;
            }
        });
        
        Validator::extend('company_name_exists_create', function($field, $value, $parameters) {
            $company_name_original = strip_tags(Input::get('company_name'));
            $check_company_name = User::where('company_name_original', $company_name_original)
                                                ->orWhereRaw("REPLACE(company_name_original, ' ', '-') = '" . str_replace(' ', '-', $company_name_original) . "'")  // should be fixed
                                                ->first();
            if((!$check_company_name && $company_name_original != 'demo' && $company_name_original != 'www') || ($check_company_name && $check_company_name->company_name != $company_name_original)) {
                return true;
            }
        });
        
        Validator::extend('company_name_exists', function($field, $value, $parameters) {
            $company_name_original = strip_tags(Input::get('company_name'));
            $check_company_name = User::where('company_name_original', $company_name_original)
                                                ->where('id', '!=', Auth::user()->get()->id)
                                                ->first();
            if(!$check_company_name && $company_name_original != 'demo' && $company_name_original != 'www') {
                return true;
            }
        });
        
        return Validator::make($input, $rules, $this->messages);
    }

    public function singleValidate($input, $field) {
        $rules = [];
        if (isset($this->rules[$field])) {
            $rules[$field] = $this->rules[$field];
            if ($field == 'email') {
                $rules[$field] = 'Required|Between:3,50|Email|unique:users,email,' . Auth::user()->get()->id . ',id';
            } elseif ($field == 'company_name') {
                Validator::extend('company_name_exists', function($field, $value, $parameters) {
                    $company_name_original = strip_tags(Input::get('value'));
                    $check_company_name = User::where('company_name_original', $company_name_original)
                                                ->where('id', '!=', Auth::user()->get()->id)
                                                ->first();
                    if(!$check_company_name && $company_name_original != 'demo' && $company_name_original != 'www') {
                        return true;
                    }
                });
                $rules[$field] = 'Required:Max:64|company_name_exists|unique:users,company_name,' . Auth::user()->get()->id . ',id';
            }
        }
        return Validator::make($input, $rules, $this->messages);
    }

    public function isEmployee() {
        return ($this->type == self::TYPE_EMPLOYEE) ? true : false;
    }

    public function isEmployer() {
        return ($this->type == self::TYPE_EMPLOYER) ? true : false;
    }

    public static function getTitleTypes() {
        return [
            self::TITLE_MR => Lang::get('label.Mr'),
            self::TITLE_MRS => Lang::get('label.Mrs'),
        ];
    }

    public function getFolderPath() {
        return 'uploads/' . self::FOLDER_PREFIX . $this->id . '/';
    }

    public function getPhoto($by_gender = false) {
        if ($this->photo) {
            return DIRECTORY_SEPARATOR . $this->getFolderPath() . $this->photo;
        } else {
            if (isset($this->title) && !empty($this->title)) {
                if ($this->title == self::TITLE_MR) {
                    return "assets/images/bg/Avatar_man.jpg";
                } else {
                    return "assets/images/bg/Avatar_Woman.jpg";
                }
            } else {
                return "assets/images/bg/default.png";
            }
        }
    }

    public function industriesRel() {
        return $this->hasMany('UserIndustry');
    }

    public function locations() {
        return $this->hasMany('UserLocation');
    }

    public function jobTitles() {
        return $this->hasMany('UserJobTitle');
    }

    public function workExperiences() {
        return $this->hasMany('UserWorkExperience');
    }

    public function educations() {
        return $this->hasMany('UserEducation');
    }

    public function highestEducation() {
        return $this->hasOne('SkillTypeValue', 'id', 'education_id');
    }

    public function documents() {
        return $this->hasMany('UserDocument');
    }

    public function skills() {
        return $this->hasMany('UserSkill');
    }

    public function languages() {
        return $this->hasMany('UserLanguage', 'user_id', 'id');
    }

    public function sms() {
        return $this->belongsTo('SendSMS', 'user_id', 'id');
    }

    public function companyProfile() {
        return $this->hasOne('CompanyProfilePage', 'user_id', 'id');
    }

    public function jobPosts() {
        return $this->hasMany('JobPost', 'user_id', 'id');
    }

    public function positions() {
        return $this->hasMany('UserPosition', 'user_id', 'id');
    }

    public function workloads() {
        return $this->hasMany('UserWorkload', 'user_id', 'id');
    }

    public function followedCompanies() {
        return $this->hasMany('FollowedCompany', 'user_id', 'id');
    }

    public function settings() {
        return $this->hasOne('UserSetting', 'user_id', 'id');
    }

    public function subscriptions() {
        return $this->hasMany('CompanySubscription', 'user_id', 'id');
    }

    public static function getStatusTypes() {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
            self::STATUS_DELETED => 'Deleted',
        ];
    }

    public static function getPostsLimits() {
        return [
            self::POSTS_LIMITED => 'Normal',
            self::POSTS_UNLIMITED => 'Unlimited jobs',
            self::POSTS_TRIAL => 'Trial'
        ];
    }
    
    public function getUserPostsLimits() {
        $postsLimits = self::getPostsLimits();
        if($this->getCreatedDays() <= 15) {
            unset($postsLimits[\User::POSTS_LIMITED]);
        } else {
            unset($postsLimits[\User::POSTS_TRIAL]);
        }
        return $postsLimits;
    }
    
    public function getCreatedDays() {
        $created = $this->created_at;
        $carbon = new \Carbon\Carbon();
        return $carbon->diffInDays($created);
    }

    public function getStatus() {
        $data = self::getStatusTypes();
        if (isset($data[$this->status])) {
            return $data[$this->status];
        }
        return 'N/A';
    }
    
    public static function getIndexStatuses() {
        return [
            self::INDEX_ON => 'On',
            self::INDEX_OFF => 'Off'
        ];
    }

    public function checkSubscription() {
        $existingSubscription = $this->subscriptions()
                ->whereRaw('"' . date('Y-m-d') . '" <= end_date')
                ->orderBy('end_date', 'DESC')
                ->first();
        if ($existingSubscription) {
            return true;
        }
        return false;
    }

    public function getSubscriptionLeftDays() {
        $existingSubscription = $this->subscriptions()
                ->orderBy('end_date', 'DESC')
                ->first();
        $end_date = new \Carbon\Carbon($existingSubscription->end_date);
        $now = \Carbon\Carbon::now();
        return $end_date->diff($now)->days + 1;
    }

    public static function randomPassword() {
        $pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, 6);
        return $pass;
    }

    public static function getMaxNumber($firstname, $lastname, $id = '') {
        $test = self::where('firstname', $firstname)
                ->where('lastname', $lastname);
        if ($id) {
            $test->where('id', '!=', $id);
        }
        return $test->max('user_number');
    }

}